$(function () {

    setDatePicker();
    setFormHandler();

    /**
     * Set send form handler
     */
    function setFormHandler() {
        $(document).on("submit", "#form", function () {
            var action = $(this).prop("action");
            $.post(
                action,
                $("#form").serialize()
            ).fail(function (data) {
                var globalError = $(".error.global");
                globalError.show();
                globalError.html(data.responseJSON.error);
                cleanGraph();
                cleanTable();
            }).done(function (data) {
                $("div.error").html("");
                if(typeof data.errors !== "undefined"){
                    showErrors(data.errors);
                } else {
                    if(typeof data.table !== "undefined"){
                        var $tableContainer = $("#table-container");
                        $("#table-view").show();
                        $tableContainer.html(data.table);
                    }
                    if(typeof data.chart !== "undefined"){
                        showChart(data.chart);
                    }
                }
            });
            return false;
        });
    }

    /**
     * Show errors
     *
     * @param errors
     */
    function showErrors(errors) {
        for (var key in errors) {
            var input = $("input[name='" + key + "']");
            var error = input.siblings(".error");
            error.html(errors[key])
        }
    }

    /**
     * Set datapicker events
     */
    function setDatePicker() {
        var dateFormat = "dd-mm-yy";
        var from = $("#datepicker-start").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            dateFormat: dateFormat
        }).on("change", function () {
            to.datepicker("option", "minDate", getDate(this, dateFormat));
        });
        var to = $("#datepicker-end").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            dateFormat: dateFormat
        }).on("change", function () {
            from.datepicker("option", "maxDate", getDate(this, dateFormat));
        });
    }

    /**
     * Returns formatted date
     *
     * @param element
     * @param dateFormat
     * @returns {*}
     */
    function getDate(element, dateFormat) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }
        return date;
    }

    /**
     * Remove table and hide view
     */
    function cleanTable() {
        $("#table-view").hide();
        $("#table-container").html();
    }

    /**
     * Remove svg and hide view
     */
    function cleanGraph() {
        var chartView = $("#chart-view");
        d3.select("svg").remove();
        chartView.find("span").remove();
        chartView.hide();
    }

    /**
     * Show chart
     *
     * @param data
     * @returns {boolean}
     */
    function showChart(data) {

        var chartView = $("#chart-view");
        d3.select("svg").remove();
        chartView.find("span").remove();
        chartView.show();
        if(data.length == 0){
            chartView.append("<span>Empty</span>");
            return false;
        }

        // set the dimensions and margins of the graph
        var margin = {top: 20, right: 20, bottom: 30, left: 50},
            width = 800 - margin.left - margin.right,
            height = 350 - margin.top - margin.bottom;

        // parse the date / time
        var parseTime = d3.timeParse("%Y-%m-%d");

        // set the ranges
        var x = d3.scaleTime().range([0, width]);
        var y = d3.scaleLinear().range([height, 0]);

        // define the 1st line
        var valueline = d3.line()
            .x(function(d) { return x(d.date); })
            .y(function(d) { return y(d.open); });

        // define the 2nd line
        var valueline2 = d3.line()
            .x(function(d) { return x(d.date); })
            .y(function(d) { return y(d.close); });

        // append the svg obgect to the body of the page
        // appends a 'group' element to 'svg'
        // moves the 'group' element to the top left margin
        var svg = d3.select("#chart-view").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

        // format the data
        data.forEach(function(d) {
            d.date = parseTime(d.date);
            d.close = +d.close;
            d.open = +d.open;
        });

        // Scale the range of the data
        x.domain(d3.extent(data, function(d) { return d.date; }));
        y.domain([0, d3.max(data, function(d) {
            return Math.max(d.close, d.open); })]);

        // Add the valueline path.
        svg.append("path")
            .data([data])
            .attr("class", "line")
            .style("stroke", "green")
            .attr("d", valueline);

        // Add the valueline2 path.
        svg.append("path")
            .data([data])
            .attr("class", "line")
            .style("stroke", "red")
            .attr("d", valueline2);

        // Add the X Axis
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        // Add the Y Axis
        svg.append("g")
            .call(d3.axisLeft(y));
    }
});