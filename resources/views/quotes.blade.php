<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="//code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://d3js.org/d3.v5.min.js"></script>
    <script type="text/javascript" src="{!! asset('js/script.js') !!}"></script>
    <link rel="stylesheet"  href="{!! asset('css/app.css') !!}">
    <title>Form</title>
</head>
<body>
    <div class="container">
        <div class="error global"></div>
        {!! Form::open(['action' => 'QuotesController@send', 'method' => 'post', 'id' => 'form']) !!}
            <div class="field">
                {!! Form::label('symbol', 'Company symbol') !!}<br>
                {!! Form::text('symbol', '', ['required' => 'required']) !!}<br>
                <div class="error"></div>
            </div>
            <div class="field">
                {!! Form::label('start_date', 'Start date') !!}<br>
                {!! Form::text('start_date', '', ['id' => 'datepicker-start', 'required' => 'required']) !!}<br>
                <div class="error"></div>
            </div>
            <div class="field">
                {!! Form::label('finish_date', 'End date') !!}<br>
                {!! Form::text('finish_date', '', ['id' => 'datepicker-end', 'required' => 'required']) !!}<br>
                <div class="error"></div>
            </div>
            <div class="field">
                {!! Form::label('email', 'Email') !!}<br>
                {!! Form::email('email', '', ['required' => 'required']) !!}<br>
                <div class="error"></div>
            </div>
            <div class="field">
                {!! Form::submit('Send', ['id' => 'submit']) !!}
            </div>
        {!! Form::close() !!}
    </div>
    <div id="table-view" style="display: none;">
        <h2>Table view</h2>
        <div id="table-container">
        </div>
    </div>
    <div id="chart-view" style="display: none;">
        <h2>Chart view</h2>
        <svg width="800" height="350"></svg>
    </div>
</body>
</html>
