<table>
    @foreach ($table as $item)
        <tr>
            @foreach ($item as $subItem)
                <td>{{ $subItem }}</td>
            @endforeach
        </tr>
    @endforeach
</table>