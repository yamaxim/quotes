<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 4/22/18
 * Time: 10:27 AM
 */

namespace Quotes\Services;

use Carbon\Carbon;
use ErrorException;
use Quotes\Exceptions\QuandlException;
use Quotes\Http\Requests\QuotesRequest;

/**
 * Service for connection to Quandl
 *
 * Class Quandl
 * @package Quotes\Services
 */
class Quandl
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var array fields to show (names)
     */
    protected static $showFieldsNames = [
        "Date", "Open", "High", "Low", "Close","Volume"
    ];

    /**
     * @var array fields to show
     */
    protected $showFields;

    /**
     * Quandl constructor.
     * @param QuotesRequest $request
     */
    public function __construct(QuotesRequest $request)
    {
        $requestData = $request->all();
        $baseUrl = env("QUANDL_BASE_URL") . $requestData["symbol"] . ".csv?";
        $startFormatted = Carbon::parse($requestData["start_date"])->format('Y-m-d');
        $finishFormatted = Carbon::parse($requestData["finish_date"])->format('Y-m-d');
        $query = http_build_query([
            "order" => "asc",
            'start_date' => $startFormatted,
            'end_date' => $finishFormatted
        ]);
        $this->url = $baseUrl.$query;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getData()
    {
        $table = [];
        $chart = [];
        $data  = [];
        try {
            if (($handle = fopen($this->url, "r")) !== false) {
                $row = 0;
                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    if($row > 0){
                        $chart[] = [
                            "date" => $data[0],
                            "open" => (float) $data[1],
                            "close" => (float) $data[4]
                        ];
                    } else {
                        $this->setShowFields($data);
                    }
                    $num = count($data);
                    $row++;
                    for ($c = 0; $c < $num; $c++) {
                        if(in_array($c, $this->showFields))
                            $table[$row][] = $data[$c];
                    }
                }
                fclose($handle);
            }
            if(sizeof((array)$table) > 1){
                $tableHtml = view("table", ["table" => $table])->render();
            } else {
                $tableHtml = "Empty";
            }
            $data["table"] = $tableHtml;
            $data["chart"] = $chart;
        } catch (ErrorException $exception) {
            throw new QuandlException();
        }
        return $data;
    }

    /**
     * Setting up column numbers to show
     *
     * @param $data
     */
    protected function setShowFields($data)
    {
        foreach($data as $key => $value){
            if(in_array($value, static::$showFieldsNames)){
                $this->showFields[$value] = $key;
            }
        }
    }
}