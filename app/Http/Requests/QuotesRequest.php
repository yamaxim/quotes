<?php

namespace Quotes\Http\Requests;

use Quotes\Rules\CompanyExist;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class QuotesRequest
 * @package Quotes\Http\Requests
 */
class QuotesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "symbol" => ["required", new CompanyExist()],
            "start_date" => "required|date|before:finish_date",
            "finish_date" => "required|date|after:start_date",
            "email" => "required|email"
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors]));
    }
}
