<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 4/21/18
 * Time: 10:53 AM
 */
namespace Quotes\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Mail;
use Quotes\Exceptions\QuandlException;
use Quotes\Http\Requests\QuotesRequest;
use Quotes\Mail\DateInterval;
use Quotes\Services\Quandl;

/**
 * Class QuotesController
 * @package Quotes\Http\Controllers
 */
class QuotesController extends Controller
{
    /**
     * @param QuotesRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function send(QuotesRequest $request): JsonResponse
    {
        try {
            $quandl = new Quandl($request);
            $data = $quandl->getData();
        } catch (QuandlException $exception) {
            return response()->json(
                ["error" => $exception->getMessage()],
                $exception->getCode()
            );
        }
        Mail::to($request->email)->send(new DateInterval($request));
        return response()->json($data, 200);
    }
}