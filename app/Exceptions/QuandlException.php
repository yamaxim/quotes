<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 4/22/18
 * Time: 3:36 PM
 */

namespace Quotes\Exceptions;

use ErrorException;

/**
 * Class QuandlException
 * @package Quotes\Exceptions
 */
class QuandlException extends ErrorException
{
    /**
     * @var int
     */
    public $code = 429;

    /**
     * @var string
     */
    public $message = 'Cannot get data from quandl';
}