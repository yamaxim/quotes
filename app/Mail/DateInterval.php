<?php

namespace Quotes\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Quotes\Http\Requests\QuotesRequest;
use Carbon\Carbon;

/**
 * Class DateInterval
 * @package Quotes\Mail
 */
class DateInterval extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    public $fromDate;

    /**
     * @var string
     */
    public $toDate;

    /**
     * @var mixed
     */
    public $subject;

    /**
     * Create a new message instance.
     *
     * @param QuotesRequest $request
     */
    public function __construct(QuotesRequest $request)
    {
        $this->fromDate = Carbon::parse($request->start_date)->format('Y-m-d');
        $this->toDate = Carbon::parse($request->finish_date)->format('Y-m-d');
        $this->subject = $request->symbol;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.dates')
            ->with([
                "from" => $this->fromDate,
                "to" => $this->toDate
            ]);
    }
}
