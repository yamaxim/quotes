<?php

namespace Quotes\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class CompanyExist
 * @package Quotes\Rules
 */
class CompanyExist implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $companies = [];
        $row = 1;
        $path = app_path() . "/../resources/csv/" . env("COMPANIES_CSV");

        if (($handle = fopen($path, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $row++;
                if($row > 2){
                    $companies[] = $data[0];
                }
            }
            fclose($handle);
        }

        return in_array($value, $companies);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Company does not exist.';
    }
}
