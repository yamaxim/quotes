<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// datepicker laravel
// https://laracasts.com/discuss/channels/general-discussion/a-date-picker?page=1

Route::get('/', function () {
    return view('quotes');
});

Route::post('send', 'QuotesController@send');